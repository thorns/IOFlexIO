 /*@@
   @file      CreateIOFlexIOdatafile.c
   @date      Mon 12 Mar 2001
   @author    Thomas Radke
   @desc
              Example program to create an unchunked IOFlexIO datafile
              with a single dataset which can be read as input data
              into Cactus.
   @enddesc
   @version   $Id$
 @@*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* FlexIO includes */
#include "IOProtos.h"
#include "IEEEIO.h"


/* the name of our sample data file */
#define DATAFILENAME  "x.ieee"

/* the number of dimensions of our sample data array and its size */
#define  NDIM         3
#define  NSIZE        20

/* the type of Cactus variable for the sample dataset (a grid function)
   This definition was taken from Cactus header files - for ease of use
   we didn't include these here. */
#define CCTK_GF                  2

/* the Cactus version that this datafile is (upwards) compatible with
   (versions below beta10 use a different timelevel scheme) */
#define CACTUS_VERSION  "4.0.b10"

/* a simple macro to do a FlexIO call with return code checking
   in case of an error it will issue an error message and exit */
#define CHECK_ERROR(flexio_call)                                              \
          {                                                                   \
            int _error_code = flexio_call;                                    \
                                                                              \
                                                                              \
            if (_error_code < 0)                                              \
            {                                                                 \
              fprintf (stderr, "ERROR: line %d: FlexIO call '%s' returned "   \
                               "error code %d\n",                             \
                                __LINE__, #flexio_call, _error_code);         \
              return (-1);                                                    \
            }                                                                 \
          }


 /*@@
   @routine    main
   @date       Mon 12 Mar 2001
   @author     Thomas Radke
   @desc
               Main routine creating a sample FlexIO datafile
   @enddesc

   @returntype int
   @returndesc
               0 for success, negative return values indicate an error
   @endreturndesc
@@*/
int main (void)
{
  void *data;
  int i, elements, dims[NDIM];
  Int timelevel, ntimelevels, grouptype;
  Int nprocs, ioproc_every, unchunked;
  Int dimsI[NDIM];
  char *varname, *groupname;
  IOFile datafile;


  /* create a datafile */
  datafile = IEEEopen (DATAFILENAME, "w");
  if (! IOisValid (datafile))
  {
    fprintf (stderr, "Could not create output file '%s'\n", DATAFILENAME);
    return (-1);
  }

  /* set the dimensions of our sample data array
     count the number of elements */
  elements = 1;
  for (i = 0; i < NDIM; i++)
  {
    dims[i] = NSIZE;
    dimsI[i] = NSIZE;
    elements *= NSIZE;
  }
  /* allocate the data array
     we are lazy here and only initialize it to zero */
  data = calloc (elements, sizeof (double));


  /**************************************************************************/
  /* write your data as a dataset into the file                             */
  /**************************************************************************/

  /* write the data */
  CHECK_ERROR (IOwrite (datafile, FLOAT64, NDIM, dims, data)); 

  /**************************************************************************/
  /* add the necessary attributes describing the dataset                    */
  /* as a Cactus grid variable                                              */
  /**************************************************************************/

  /* the variable's full name (as specified in the interface.ccl file) */
  varname = "grid::x";
  CHECK_ERROR (IOwriteAttribute (datafile, "name", CHAR,
                                 strlen (varname) + 1, varname));

  /* the variable's group name (as specified in the interface.ccl file) */
  groupname = "grid::coordinates";
  CHECK_ERROR (IOwriteAttribute (datafile, "groupname", CHAR,
                                 strlen (groupname) + 1, groupname));

  /* the variable's group type (as specified in the interface.ccl file) */
  grouptype = CCTK_GF;
  CHECK_ERROR (IOwriteAttribute (datafile, "grouptype", INT32, 1, &grouptype));

  /* the number of timelevels of the variable (as specified in the
     interface.ccl file) */
  ntimelevels = 1;
  CHECK_ERROR (IOwriteAttribute (datafile, "ntimelevels", INT32, 1,
                                 &ntimelevels));

  /* the timelevel of the variable - 0 for the current timelevel */
  timelevel = 0;
  CHECK_ERROR (IOwriteAttribute (datafile, "timelevel", INT32, 1, &timelevel));

  /* the dimensions of the variable (must correspond to the grid size
     in your parameter file) */
  CHECK_ERROR (IOwriteAttribute (datafile, "global_size", INT32, NDIM, dimsI));


  /**************************************************************************/
  /* add the attributes describing how the data in this file was written    */
  /* these attributes must be attached to the first dataset in the file     */
  /**************************************************************************/

  /* we are writing unchunked data */
  unchunked = 1;
  CHECK_ERROR (IOwriteAttribute (datafile, "unchunked", INT32, 1, &unchunked));

  /* the number of processors isn't really needed here
     (only for chunked data) */
  nprocs = 1;
  CHECK_ERROR (IOwriteAttribute (datafile, "GH$nprocs", INT32, 1, &nprocs));

  /* the number of I/O processors isn't really needed here
     (only for chunked data) */
  ioproc_every = 1;
  CHECK_ERROR (IOwriteAttribute (datafile, "GH$ioproc_every", INT32, 1,
                                 &ioproc_every));

  /* the version of Cactus that this datafile is (upwards) compatible with */
  CHECK_ERROR (IOwriteAttribute (datafile, "Cactus version", CHAR,
               strlen (CACTUS_VERSION) + 1, CACTUS_VERSION));


  /* close the file and free allocated resources */
  CHECK_ERROR (IOclose (datafile));
  free (data);

  return (0);
}
