 /*@@
   @header    IOFlexIOGH.h
   @date      Tue 9th Jan 1999
   @author    Gabrielle Allen
   @desc
              The extensions to the GH structure from IOFlexIO.
   @enddesc
   @version   $Header$
 @@*/

#ifndef _IOFLEXIO_IOFLEXGH_H_
#define _IOFLEXIO_IOFLEXGH_H_ 1

#include "StoreNamedData.h"
#include "CactusBase/IOUtil/src/ioutil_Utils.h"
#include "CactusExternal/FlexIO/src/IOProtos.h"
#include "CactusExternal/FlexIO/src/IEEEIO.h"


/* define the IOFlexIO datatypes according to CCTK_??? datatypes */
#define FLEXIO_BYTE    UINT8

#ifdef  CCTK_INT1
#define FLEXIO_INT1    INT8
#endif
#ifdef  CCTK_INT2
#define FLEXIO_INT2    INT16
#endif
#ifdef  CCTK_INT4
#define FLEXIO_INT4    INT32
#endif
#ifdef  CCTK_INT8
#define FLEXIO_INT8    INT64
#endif

#ifdef  CCTK_REAL4
#define FLEXIO_REAL4   FLOAT32
#endif
#ifdef  CCTK_REAL8
#define FLEXIO_REAL8   FLOAT64
#endif
#ifdef  CCTK_REAL16
#define FLEXIO_REAL16  -1
#endif

/* define the FlexIO types for the generic CCTK_INT and CCTK_REAL datatypes */
#ifdef  CCTK_INTEGER_PRECISION_8
#define FLEXIO_INT     INT64
#elif   CCTK_INTEGER_PRECISION_4
#define FLEXIO_INT     INT32
#elif   CCTK_INTEGER_PRECISION_2
#define FLEXIO_INT     INT16
#elif   CCTK_INTEGER_PRECISION_1
#define FLEXIO_INT     INT8
#endif

#ifdef  CCTK_REAL_PRECISION_4
#define FLEXIO_REAL    FLEXIO_REAL4
#elif   CCTK_REAL_PRECISION_8
#define FLEXIO_REAL    FLEXIO_REAL8
#elif   CCTK_REAL_PRECISION_16
#define FLEXIO_REAL    FLEXIO_REAL16
#endif


/* attribute name for storing parameters
   Parameters are stored as a single string attached to the very first dataset
   in an IEEEIO file. */
#define GLOBAL_PARAMETERS  "global_parameters"

/* check error flags from IOFlexIO */
#define FLEXIO_ERROR(fn_call)                                                 \
        {                                                                     \
          int _retval = fn_call;                                              \
                                                                              \
                                                                              \
          if (_retval < 0)                                                    \
          {                                                                   \
            CCTK_VWarn (1, __LINE__, __FILE__, CCTK_THORNSTRING,              \
                        "IEEEIO call '%s' returned error code %d\n",          \
                        #fn_call, _retval);                                   \
          }                                                                   \
        }


/* info structure describing IEEEIO output files
   For each open file there is one entry in the file database. */
typedef struct
{
  IOFile iofile;
  char *filename;
} IEEEfile_t;


/* structure holding necessary information about a recovery file */
typedef struct
{
  int is_IEEEIO_file;       /* flag indicating valid file info */
  IOFile fid;               /* IEEEIO file handle */
  char *filename;           /* complete file name for recovery */
  CCTK_INT nprocs;          /* number of total processors */
  CCTK_INT ioproc;          /* the associated IO processor */
  CCTK_INT ioproc_every;    /* how many IO processors there are */
  CCTK_INT unchunked;       /* whether data was written chunked or unchunked */
  int has_version;          /* whether whether file contains a Cactus version ID
                               (this is used to distinguish checkpoint files
                                with old/new timelevel naming scheme) */
} fileinfo_t;


/* IOFlexIO GH extension structure */
typedef struct IOFlexIOGH
{
  /* default number of times to output */
  int out_every_default, out2D_every_default;

  /* number of times to output for each variable */
  CCTK_INT *out_every, *out2D_every;

  /* the last iteration output for each variable */
  int *out_last, *out2D_last;

  /* list of variables to output */
  char *out_vars, *out2D_vars;

  /* I/O request descriptions for all CCTK variables */
  ioRequest **requests;

  /* directory in which to output */
  char *out_dir, *out2D_dir;

  /* filename database for opened files */
  pNamedData *fileList, *fileList_2D;

  /* timer array for checkpointing/recovery */
  int timers[4];

  /* flag to indicate request for timer output */
  int print_timing_info;

  /* 2D slices */
  int **sp2xyz;

  /* ring buffer for list of successfully created cp files */
  int    checkpoint_keep;
  int    cp_fileindex;
  char **cp_filenames;

  /* iteration number of the last checkpoint */
  int last_checkpoint_iteration;

  /* stop on I/O parameter parsing errors ? */
  int stop_on_parse_errors;

} flexioGH;


/* prototypes of functions to be registered */
int IOFlexIO_Output2DGH (const cGH *GH);
int IOFlexIO_TriggerOutput2D (const cGH *GH, int);
int IOFlexIO_TimeFor2D (const cGH *GH, int);
int IOFlexIO_Output2DVarAs (const cGH *GH, const char *var, const char *alias);
int IOFlexIO_OutputGH (const cGH *GH);
int IOFlexIO_TriggerOutput (const cGH *GH, int);
int IOFlexIO_TimeFor (const cGH *GH, int);
int IOFlexIO_OutputVarAs (const cGH *GH, const char *var, const char *alias);

/* other function prototypes */
int IOFlexIO_DataType (int cctk_type);
int IOFlexIO_Write2D (const cGH *GH, int vindex, const char *alias);
int IOFlexIO_Write (const cGH *GH, int vindex, const char *alias);
int IOFlexIO_DumpVar (const cGH *GH, const ioRequest *request, IOFile file);
void IOFlexIOi_DumpParameters (const cGH *GH, int all, IOFile file);
void IOFlexIOi_DumpGHExtensions (const cGH *GH, IOFile file);
int IOFlexIO_Recover (cGH *GH, const char *basefilename, int called_from);
int IOFlexIOi_RecoverVariables (cGH *GH, fileinfo_t *file);
void IOFlexIO_CheckSteerableParameters (const cGH *GH, flexioGH *myGH);
void IOFlexIO_CheckSteerableParameters2D (flexioGH *myGH);
#endif  /* _IOFLEXIO_IOFLEXGH_H_ */
