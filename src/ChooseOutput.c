 /*@@
   @file      ChooseOutput.c
   @author    Gabrielle Allen
   @date      July 6 2000
   @desc
              Choose what data to write for different I/O methods in IOFlexIO
   @enddesc 
   @version   $Id$
@@*/

#include <stdlib.h>

#include "cctk.h"
#include "cctk_Parameters.h"
#include "CactusBase/IOUtil/src/ioutil_Utils.h"
#include "ioFlexGH.h"

/* the rcs ID and its dummy function to use it */
static const char *rcsid = "$Header$";
CCTK_FILEVERSION(CactusPUGHIO_IOFlexIO_ChooseOutput_c)


/********************************************************************
 ********************    Macro Definitions   ************************
 ********************************************************************/
/* macro to choose origin according actual parameter settings:
     1. Indices from IOFlexIO
     2. Indices from IOUtil
     3. Coords from IOFlexIO
     4. Coords from IOUtil
 */
#define GET_SLICE(IOFlexIO_param, IOUtil_param, index, coord)                 \
        {                                                                     \
          index = IOFlexIO_param##i >= 0 ? IOFlexIO_param##i : IOUtil_param##i;\
          coord = IOFlexIO_param != -424242 ? IOFlexIO_param : IOUtil_param;  \
        }


/********************************************************************
 ********************    External Routines   ************************
 ********************************************************************/
void IOFlexIO_Choose2D (const cGH *GH);


 /*@@
   @routine   IOFlexIO_Choose2D
   @author    Gabrielle Allen
   @date      July 6 2000
   @desc
              Use parameters to choose the 2D planes through the output data. 
   @enddesc

   @calls     IOUtil_2DPlanes

   @var       GH
   @vdesc     pointer to CCTK grid hierarchy
   @vtype     const cGH *
   @vio       in
   @endvar
@@*/
void IOFlexIO_Choose2D (const cGH *GH)
{
  int i, maxdim;
  flexioGH *myGH;
  int origin_index[3];
  CCTK_REAL origin_phys[3];
  DECLARE_CCTK_PARAMETERS


  GET_SLICE (out2D_xyplane_z, out_xyplane_z, origin_index[0], origin_phys[0]);
  GET_SLICE (out2D_xzplane_y, out_xzplane_y, origin_index[1], origin_phys[1]);
  GET_SLICE (out2D_yzplane_x, out_yzplane_x, origin_index[2], origin_phys[2]);

  maxdim = CCTK_MaxDim ();
  myGH = (flexioGH *) CCTK_GHExtension (GH, "IOFlexIO");
  myGH->sp2xyz = (int **) malloc (3 * sizeof (int *));

  for (i = 0; i < maxdim; i++)
  {
    myGH->sp2xyz[i] = (int *) calloc (i + 1, sizeof (int));

    if (i > 0 && i < 3)
    {
      IOUtil_2DPlanes (GH, i + 1, origin_index, origin_phys, myGH->sp2xyz[i]);
    }
  }
}
