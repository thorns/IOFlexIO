 /*@@
   @file      ieee_merge.c
   @date      Fri 14 Dec 2001
   @author    Thomas Radke
   @desc
              Utility program to merge IEEEIO datafiles.
   @enddesc
   @version   $Id$
 @@*/

#define  MAXDIM    3
#define  MAXNAMESIZE  100

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/* CCTK includes */
#include "cctk.h"

/* FlexIO includes */
#include "IOProtos.h"
#include "IEEEIO.h"

/* the rcs ID and its dummy function to use it */
static const char *rcsid = "$Header$";
CCTK_FILEVERSION(CactusPUGHIO_IOFlexIO_util_ieee_merge_c)


int main (int argc, char **argv)
{
  int h, i, j;
  IOFile infile, *infiles, outfile;
  int nDatasets, nAttributes, nofInfiles;
  int datatype, out_datatype;
  int rank, out_rank;
  int dims[MAXDIM], out_dims[MAXDIM];
  int attrType, attrLen;
  int global_size[MAXDIM], out_global_size[MAXDIM];
  char name[MAXNAMESIZE], out_name[MAXNAMESIZE];
  char attrName[MAXNAMESIZE];
  void *data, *attrData;


  if (argc <= 2)
  {
    printf ("Usage: %s <outfile> <infile1> <infile2> .. <infileN>\n", argv[0]);
    return (0);
  }

  out_rank = 0;
  out_datatype = 0;
  outfile = IEEEopen (argv[1], "w");
  if (! IOisValid (outfile))
  {
    printf ("Could not open output file '%s'\n", argv[1]);
    return (1);
  }
  nofInfiles = argc - 2;
  infiles = (IOFile *) malloc (nofInfiles * sizeof (IOFile));
  for (i = 0; i < nofInfiles; i++)
  {
    infiles[i] = IEEEopen (argv[i + 2], "r");
    if (! IOisValid (infiles[i]))
    {
      printf ("Could not open input file '%s'\n", argv[i + 2]);
      return (1);
    }
  }

  for (h = 0; h < nofInfiles; h++)
  {
    infile = infiles[h];
    nDatasets = IOnDatasets (infile);
    printf ("Input file '%s' contains %d datasets\n", argv[h + 2], nDatasets);

    for (i = 0; i < nDatasets; i++)
    {
      if (IOreadInfo (infile, &datatype, &rank, dims, MAXDIM) <= 0)
      {
        printf ("Cannot read info of datatset %d\n", i);
        continue;
      }
      if (h > 0 && (datatype != out_datatype || rank != out_rank ||
                    dims[0] != out_dims[0] || dims[1] != out_dims[1] ||
                    dims[2] != out_dims[2]))
      {
        printf ("Dataset type or dims differ\n");
        continue;
      }

      /* retrieve name and iteration attribute of dataset */
      strcpy (attrName, "name");
      j = IOreadAttributeInfo (infile, attrName, &attrType, &attrLen);
      if (j < 0)
      {
        printf ("Cannot find name attribute of dataset %d\n", i);
        continue;
      }
      if (IOreadAttribute (infile, j, name) < 0)
      {
        printf ("Cannot read name attribute of dataset %d\n", i);
        continue;
      }
      if (h > 0 && strcmp (name, out_name))
      {
        printf ("Attribute 'name' differs !\n");
        continue;
      }

#if  0
      strcpy (attrName, "iteration");
      j = IOreadAttributeInfo (infile, attrName, &attrType, &attrLen);
      if (j < 0)
      {
        printf ("Cannot find iteration attribute of dataset %d\n", i);
        continue;
      }
      if (IOreadAttribute (infile, j, &iteration) < 0)
      {
        printf ("Cannot read iteration attribute of dataset %d\n", i);
        continue;
      }
#endif

      strcpy (attrName, "global_size");
      j = IOreadAttributeInfo (infile, attrName, &attrType, &attrLen);
      if (j < 0)
      {
        printf ("Cannot find global_size attribute of dataset %d\n", i);
        continue;
      }
      if (IOreadAttribute (infile, j, global_size) < 0)
      {
        printf ("Cannot read global_size attribute of dataset %d\n", i);
        continue;
      }
      if (h > 0 && (global_size[0] != out_global_size[0] ||
          global_size[1] != out_global_size[1] ||
          global_size[2] != out_global_size[2]))
      {
        printf ("Attribute 'global_size' differs !\n");
        continue;
      }

      if (h == 0)
      {
        out_datatype = datatype;
        out_rank = rank;
        out_dims[0] = dims[0];
        out_dims[1] = dims[1];
        out_dims[2] = dims[2];
        strcpy (out_name, name);
        out_global_size[0] = global_size[0];
        out_global_size[1] = global_size[1];
        out_global_size[2] = global_size[2];
      }

      data = malloc (IOnBytes (datatype, rank, dims));
      IOread (infile, data);
      IOwrite (outfile, datatype, rank, dims, data);
      free (data);

      nAttributes = IOnAttributes (infile);
      printf ("Processing dataset '%s' with %d attributes\n", name,nAttributes);

      for (j = 0; j < nAttributes; j++)
      {
        if (IOreadIndexedAttributeInfo (infile, j, attrName, &attrType,
                                        &attrLen, MAXNAMESIZE) < 0)
        {
#if  0
          /* it's only a bug if this returns -1 */
          printf ("Cannot read info of attribute %d, skipping ...\n", j);
          continue;
#endif
        }

        /* for string attributes: allocate one more bytes for trailing NUL */
        attrData = malloc (IOnBytes (attrType, 1, &attrLen) + 1);
        if (IOreadAttribute (infile, j, attrData) < 0)
        {
          printf ("Cannot read value of attribute %d, skipping ...\n", j);
          continue;
        }
        IOwriteAttribute (outfile, attrName, attrType, attrLen, attrData);
        free (attrData);
      }

    }

    IOclose (infile);
  }

  IOclose (outfile);

  free (infiles);

  return (0);
}
