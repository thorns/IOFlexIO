 /*@@
   @file      ieee_convert_from_cactus3.c
   @date      Fri 14 Dec 2001
   @author    Thomas Radke
   @desc
              Utility program to convert Cactus 3 IEEEIO datafiles into
              Cactus 4 IEEEIO datafiles.
   @enddesc
   @version   $Id$
 @@*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* CCTK includes */
#include "cctk.h"

/* FlexIO includes */
#include "IOProtos.h"
#include "IEEEIO.h"

/* the rcs ID and its dummy function to use it */
static const char *rcsid = "$Header$";
CCTK_FILEVERSION(CactusPUGHIO_IOFlexIO_util_ieee_convert_from_cactus3_c)


#define MAXDIM          3           /* max. dimensions of an IEEEIO dataset  */
#define MAXNAMESIZE     100         /* max. attribute name length            */
#define MAXLINELEN      200         /* longest linesize in the mapfile       */
#define MAXMAPPINGS     1000        /* max. lines of (valid) mapfile entries */

typedef struct                      /* a mapping description as it is read   */
{                                   /* from the mapfile                      */
  char *oldname;
  char *newname;
  char *groupname;
} map_desc_t;


int main (int argc, char **argv)
{
  int i, j;
  IOFile infile, outfile;
  int nDatasets, nAttributes;
  int datatype;
  int rank;
  int dims[MAXDIM];
  int attrType, attrLen;
  char attrName[MAXNAMESIZE];
  void *data, *attrData;
  FILE *mapfile;
  int nMappings;
  map_desc_t *mapping[MAXMAPPINGS];
  char line[MAXLINELEN];


  if (argc != 4)
  {
    printf ("Usage: %s <mapfile>  <infile> <outfile>\n\n", argv[0]);
    printf ("          where <mapfile> contains mappings for variable names\n");
    printf ("          from Cactus 3.x to Cactus 4.0 in the format\n");
    printf ("            <old varname> -> <full varname> <full groupname>\n");
    printf ("          eg.\n");
    printf ("              x   -> grid::x        grid::coordinates\n");
    printf ("              y   -> grid::y        grid::coordinates\n");
    printf ("              z   -> grid::z        grid::coordinates\n");
    printf ("              alp -> einstein::alp  einstein::lapse\n\n");
    return (0);
  }

  mapfile = fopen (argv[1], "r");
  if (! mapfile)
  {
    printf ("ERROR: Could not open mapfile '%s'\n\n", argv[1]);
    return (-1);
  }
  nMappings = i = 0;
  while (fgets (line, MAXLINELEN - 1, mapfile) != NULL)
  {
    char oldname[MAXLINELEN], newname[MAXLINELEN];
    char groupname[MAXLINELEN];


    i++;
    if (line[strlen (line) - 1] == '\n')
    {
      line[strlen (line) - 1] = 0;
    }
    if (sscanf (line, "%s -> %s %s", oldname, newname, groupname) != 3)
    {
      printf ("Ignoring line %d in mapfile: '%s'\n", i, line);
    }
    else
    {
      mapping[nMappings] = (map_desc_t *) malloc (sizeof (map_desc_t));
      mapping[nMappings]->oldname = strdup (oldname);
      mapping[nMappings]->newname = strdup (newname);
      mapping[nMappings]->groupname = strdup (groupname);
      nMappings++;
    }
  }
  fclose (mapfile);
  if (nMappings == 0)
  {
    printf ("ERROR: Could not read any name mapping from '%s'\n", argv[1]);
    return (-1);
  }

  infile = IEEEopen (argv[2], "r");
  if (! IOisValid (infile))
  {
    printf ("ERROR: Could not open input file '%s'\n\n", argv[2]);
    return (1);
  }
  nDatasets = IOnDatasets (infile);
  printf ("Input file '%s' contains %d datasets\n", argv[2], nDatasets);

  outfile = IEEEopen (argv[3], "w");
  if (! IOisValid (outfile))
  {
    printf ("ERROR: Could not open output file '%s'\n\n", argv[3]);
    return (1);
  }

  for (i = 0; i < nDatasets; i++)
  {
    if (IOreadInfo (infile, &datatype, &rank, dims, MAXDIM) <= 0)
    {
      printf ("ERROR: Cannot read info of datatset %d, skipping dataset\n\n",i);
      continue;
    }

    data = malloc (IOnBytes (datatype, rank, dims));
    IOread (infile, data);
    IOwrite (outfile, datatype, rank, dims, data);
    free (data);

    nAttributes = IOnAttributes (infile);
    printf ("Processing dataset %d with %d attributes\n", i, nAttributes);

    for (j = 0; j < nAttributes; j++)
    {
      if (IOreadIndexedAttributeInfo (infile, j, attrName, &attrType,
                   &attrLen, MAXNAMESIZE) < 0)
      {
#if  0
        /* it's only a bug if this returns -1 */
        printf ("Cannot read info of attribute %d, "
            "skipping ...\n", j);
        continue;
#endif
      }
      attrData = malloc (IOnBytes (attrType, 1, &attrLen));
      if (IOreadAttribute (infile, j, attrData) < 0)
      {
        printf ("ERROR: Cannot read value of attribute %d, "
                "skipping attribute...\n\n", j);
        continue;
      }

      if (! strcmp (attrName, "name"))
      {
        int k;
        Int grouptype = CCTK_GF;    /* only grid functions have a 'name' attr.*/
        Int ntimelevels = 1;        /* Cactus 3.x had no timelevels */
        Int timelevel = 0;

        for (k = 0; k < nMappings; k++)
        {
          if (! strcmp (attrData, mapping[k]->oldname))
          {
            printf ("Found grid function with name '%s', \n"
                    "  set it to new varname '%s' with groupname '%s'\n",
                    mapping[k]->oldname, mapping[k]->newname,
                    mapping[k]->groupname);

            /* add the groupname for this variable */
            IOwriteAttribute (outfile, "groupname", BYTE,
              strlen (mapping[k]->groupname) + 1, mapping[k]->groupname);

            /* write the implicite Cactus 4.0 attributes */
            IOwriteAttribute (outfile, "grouptype", INT32, 1, &grouptype);
            IOwriteAttribute (outfile, "ntimelevels", INT32, 1, &ntimelevels);
            IOwriteAttribute (outfile, "timelevel", INT32, 1, &timelevel);

            /* and set the data pointer to use the new variable name */
            free (attrData);
            attrData = (char *) strdup (mapping[k]->newname);
            attrLen = strlen (mapping[k]->newname) + 1;
            break;
          }
        }
      }

      if (! strcpy (attrName, "GH$GHiteration"))
      {
        /* copy this also to the "main loop iteration" attribute */
        IOwriteAttribute (outfile, "main loop iteration", attrType, attrLen,
                          attrData);
        /* and map "GH$GHiteration" to "GH$iteration" */
        strcpy (attrName, "GH$iteration");
      }

      if (! strcpy (attrName, "GH$phys_time"))
      {
        /* map "GH$phys_time" to "GH$time" */
        strcpy (attrName, "GH$time");
      }

      IOwriteAttribute (outfile, attrName, attrType, attrLen, attrData);
      free (attrData);
    }
  }

  IOclose (infile);
  IOclose (outfile);

  for (i = 0; i < nMappings; i++)
  {
    free (mapping[i]->oldname);
    free (mapping[i]->newname);
    free (mapping[i]);
  }

  return (0);
}
